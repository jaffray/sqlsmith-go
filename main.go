package main

import "github.com/cockroachdb/sqlsmith-go/sqlsmith"

func main() {
	sqlsmith.Run()
}
